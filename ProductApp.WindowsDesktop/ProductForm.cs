using ProductApp.WindowsDesktop.Models;

namespace ProductApp.WindowsDesktop
{
    public partial class ProductForm : Form
    {
        static int counter = 0;
        public ProductForm()
        {
            InitializeComponent();
        }

        private void lblName_Click(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Product product = new Product()
            {
                Id = counter + 1,
                Name = lblName.Text,
                Description = lblDescription.Text,
                isAvailable = domainUpDownIsAvailable.Text,
            };
            counter++;
            MessageBox.Show($"{product}");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}